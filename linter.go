package linters

import (
	"fmt"
	"go/ast"
	"go/token"

	"golang.org/x/tools/go/analysis"
	"golang.org/x/tools/go/analysis/passes/inspect"
	"golang.org/x/tools/go/ast/inspector"
)

var GoArgsAnalyzer = &analysis.Analyzer{
	Name:     "goargs",
	Doc:      "Checks for consistent formatting on argument lists.",
	Run:      run,
	Requires: []*analysis.Analyzer{inspect.Analyzer},
}

func run(pass *analysis.Pass) (interface{}, error) {
	//nolint:errcheck
	inspector := pass.ResultOf[inspect.Analyzer].(*inspector.Inspector)
	nodeFilter := []ast.Node{
		(*ast.FuncDecl)(nil),
		(*ast.CallExpr)(nil),
	}

	inspector.Preorder(nodeFilter, func(node ast.Node) {
		switch v := node.(type) {
		case *ast.FuncDecl:
			checkFuncDecl(v, pass)
		case *ast.CallExpr:
			checkCallExpr(v, pass)
		}
	})

	return nil, nil
}

func checkFuncDecl(funcDecl *ast.FuncDecl, pass *analysis.Pass) {
	params := funcDecl.Type.Params
	paramsList := params.List
	if len(paramsList) == 0 {
		return
	}

	openingPos := pass.Fset.Position(params.Opening)
	closingPos := pass.Fset.Position(params.Closing)
	allItemsOnSameLine := openingPos.Line == closingPos.Line
	if allItemsOnSameLine {
		return
	}

	// Check if all arguments could fit in one line
	if checkGA003(funcDecl, pass, openingPos, params) {
		return
	}

	// Check if each argument is in its own line
	checkGA001(openingPos, paramsList, pass)

	// Check if the closing token is in its own line
	checkGA002(funcDecl, pass, params, paramsList)
}

func checkGA001(openingPos token.Position, paramsList []*ast.Field, pass *analysis.Pass) {
	prevParamPos := openingPos
	for _, param := range paramsList {
		paramPos := pass.Fset.Position(param.Pos())
		if paramPos.Line != prevParamPos.Line+1 {
			pass.Report(
				analysis.Diagnostic{
					Pos: param.Names[0].Pos(),
					Message: fmt.Sprintf(
						"GA001: arguments should be either all in a single line, "+
							"or one argument per-line. `%s` should be in its own line",
						param.Names[0].String(),
					),
					Category: "Style",
				})
			break
		}
		prevParamPos = pass.Fset.Position(param.End())
	}
}

func checkGA002(funcDecl *ast.FuncDecl, pass *analysis.Pass, params *ast.FieldList, paramsList []*ast.Field) {
	closingPos := pass.Fset.Position(params.Closing)
	finalParamPos := pass.Fset.Position(paramsList[len(paramsList)-1].End())
	if finalParamPos.Line == closingPos.Line {
		pass.Report(
			analysis.Diagnostic{
				Pos: params.Closing,
				Message: fmt.Sprintf(
					"GA002: should add a final comma and newline "+
						"before the closing of the parameter list to `%s`",
					funcDecl.Name.String()),
				Category: "Style",
			})
	}
}

func checkGA003(funcDecl *ast.FuncDecl, pass *analysis.Pass, openingPos token.Position, params *ast.FieldList) bool {
	funcTypeText := getFuncTypeAsString(pass, funcDecl.Type, funcDecl.Name != nil)
	if openingPos.Column+len(funcTypeText) < 120 {
		diagnostic := analysis.Diagnostic{
			Pos:      params.Opening,
			End:      funcDecl.Type.End(),
			Category: "Style",
			Message: fmt.Sprintf(
				"GA003: expression call fits in a single line, replace it with `%s`",
				funcTypeText,
			),
			SuggestedFixes: []analysis.SuggestedFix{
				{
					Message: "join all arguments into a single line",
					TextEdits: []analysis.TextEdit{
						{
							Pos:     params.Opening,
							End:     funcDecl.Body.Lbrace,
							NewText: []byte(funcTypeText),
						},
					},
				},
			},
		}

		pass.Report(diagnostic)
		return true
	}

	return false
}

func checkCallExpr(callExpr *ast.CallExpr, pass *analysis.Pass) {
	args := callExpr.Args
	if len(args) == 0 {
		return
	}

	if singleLineCallExpr(callExpr, pass, args) {
		return
	}

	// Check if the first argument is split into a new line
	checkGA004(args, pass, pass.Fset.Position(callExpr.Lparen))

	// Check if the closing token is in its own line
	checkGA005(callExpr, pass, args)
}

func singleLineCallExpr(callExpr *ast.CallExpr, pass *analysis.Pass, args []ast.Expr) bool {
	lParenPos := pass.Fset.Position(callExpr.Lparen)
	allItemsOnSameLine := true
	prevArgPos := lParenPos
	for _, arg := range args {
		argPos := pass.Fset.Position(arg.Pos())
		if argPos.Line != prevArgPos.Line {
			allItemsOnSameLine = false
			break
		}
		prevArgPos = pass.Fset.Position(arg.End())
	}
	return allItemsOnSameLine
}

func checkGA004(args []ast.Expr, pass *analysis.Pass, lParenPos token.Position) {
	firstArg := args[0]
	argPos := pass.Fset.Position(firstArg.Pos())
	if argPos.Line == lParenPos.Line {
		pass.Report(
			analysis.Diagnostic{
				Pos:      firstArg.Pos(),
				End:      firstArg.End(),
				Message:  "GA004: the first argument should be broken into a new line",
				Category: "Style",
			})
	}
}

func checkGA005(callExpr *ast.CallExpr, pass *analysis.Pass, args []ast.Expr) {
	lastArg := args[len(args)-1]
	lastArgPos := pass.Fset.Position(lastArg.Pos())
	rParenPos := pass.Fset.Position(callExpr.Rparen)
	if rParenPos.Line == lastArgPos.Line {
		pass.Report(
			analysis.Diagnostic{
				Pos:      callExpr.Rparen,
				Message:  "GA005: should add a final comma and newline before the closing of the argument list",
				Category: "Style",
			})
	}
}
